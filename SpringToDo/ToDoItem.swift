//
//  ToDoItem.swift
//  SpringToDo
//
//  Created by Steve Greenley on 16/01/16.
//  Copyright © 2016 Swiftly Mobile. All rights reserved.
//

import Foundation
import CoreData


class ToDoItem: NSManagedObject {
  static let entityName = "ToDoItem"
}
