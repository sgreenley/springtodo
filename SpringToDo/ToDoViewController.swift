//
//  ViewController.swift
//  SpringToDo
//
//  Created by Steve Greenley on 14/01/16.
//  Copyright © 2016 Swiftly Mobile. All rights reserved.
//

import UIKit
import CoreData

class ToDoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ToDoItemTVCDelegate {

  // The main screen has two modes indicated by this flag
  // Show or hide completed items
  var showCompletedItemsFlag: Bool = false

  // The data for this screen
  var toDoItems: [ToDoItem] = []

  var appDelegate: AppDelegate!
  var managedContext: NSManagedObjectContext!

  // MARK: Outlets
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var clearCompletedItemsButton: UIBarButtonItem!
  @IBOutlet weak var showHideCompletedButton: UIButton!
  @IBOutlet weak var addItemButton: UIBarButtonItem!
  
  // MARK: Actions
  
  @IBAction func toggleShowCompleted(sender: AnyObject) {
    showCompletedItemsFlag = !showCompletedItemsFlag
    
    if (showCompletedItemsFlag) {
      self.title = "Completed Items"
      showHideCompletedButton.setTitle("Show To Do List", forState: .Normal)
    } else {
      self.title = "To Do"
      showHideCompletedButton.setTitle("Show Completed", forState: .Normal)
    }
    clearCompletedItemsButton.enabled = showCompletedItemsFlag
    addItemButton.enabled = !showCompletedItemsFlag
    tableView.reloadData()
  }
  
  @IBAction func clearCompletedItems(sender: AnyObject) {
    // TODO toDoItems = toDoItems.filter() { !$0.completed }
    for toDoItem in toDoItems {
      if (toDoItem.completed == true) {
        managedContext.deleteObject(toDoItem)
      }
    }
    saveManagedContext()
    loadToDoList()
    tableView.reloadData()
  }
  
  @IBAction func addItem(sender: AnyObject) {
    let alert = UIAlertController(title: "Add New Item",
      message: "What do you need to do?",
      preferredStyle: .Alert)
   
    let addToDoItemAction = UIAlertAction(title: "Add",
      style: .Default,
      handler: { (action:UIAlertAction) -> Void in
        // Add a ToDoItem
        let textField = alert.textFields!.first
        self.createToDoItem(textField!.text!, completed: false)        
        self.loadToDoList()
        self.tableView.reloadData()
    })
   
    let cancelAction = UIAlertAction(title: "Cancel",
      style: .Default) {
      (action: UIAlertAction) -> Void in // do nothing for now
    }
   
    alert.addTextFieldWithConfigurationHandler {
      (textField: UITextField) -> Void in
        textField.autocapitalizationType = .Sentences
    }
   
    alert.addAction(addToDoItemAction)
    alert.addAction(cancelAction)
   
    presentViewController(alert,
      animated: true,
      completion: nil)
  }

  // MARK: ViewController lifecycle methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    self.managedContext = appDelegate.managedObjectContext
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    loadToDoList()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  // MARK: TableView DataSource methods
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let CellIndentifier: NSString = "ToDoItemCell"
    let cell = tableView.dequeueReusableCellWithIdentifier(CellIndentifier as String)! as! ToDoItemTableViewCell
    cell.delgate = self

    let toDoItem = self.toDoItems[indexPath.row]
    cell.toDoItem = toDoItem
    cell.rowNumber = indexPath.row
    cell.hidden = toDoItem.completed != showCompletedItemsFlag
    return cell
  }
  
  func shouldBeHidden(completed: Bool, showCompleted: Bool) -> Bool {
    return (completed && !showCompleted) || (!completed && showCompleted)
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }

  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.toDoItems.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    let defaultCellHeight:CGFloat = 44
    let toDoItem = self.toDoItems[indexPath.row]
    return toDoItem.completed != showCompletedItemsFlag ? 0 : defaultCellHeight
  }
  
  // MARK: TodoItemTVCDelegate methods
  
  func setStateOfToDoItemAtRow(row: Int, completed: Bool) {
    toDoItems[row].completed = completed
    saveManagedContext()
    animateChangeToTableView(tableView)
  }
  
  // Data persistence
  
  // loads toDoItems array
  func loadToDoList() {
    let fetchRequest = NSFetchRequest(entityName: ToDoItem.entityName)
    fetchRequest.sortDescriptors = [
      NSSortDescriptor(key: "creationDate", ascending: false)
    ]
    do {
      try toDoItems = managedContext.executeFetchRequest(fetchRequest) as! [ToDoItem]
    } catch let error as NSError {
      print("Could not update ToDoItem \(error), \(error.userInfo)")
    }
  }
  
  // Saves all objects associated with our managed context
  func saveManagedContext() {
    do {
      try managedContext.save()
    } catch let error as NSError  {
        print("Could not save data \(error), \(error.userInfo)")
    }
  }
  
  // Helper method
  
  func createToDoItem(itemText:String, completed:Bool) {
    let newToDoItem = NSEntityDescription.insertNewObjectForEntityForName(ToDoItem.entityName,
      inManagedObjectContext: self.managedContext) as! ToDoItem
    newToDoItem.itemText = itemText
    newToDoItem.completed = completed
    newToDoItem.creationDate = NSDate()
    self.saveManagedContext()
  }
  
  func animateChangeToTableView(tableView:UITableView) {
    // Causes the table view to show any updates with animation
    // As mentioned in Apple's WWDC 2010 "Mastering Table View" session
    tableView.beginUpdates()
    tableView.endUpdates()
  }
  
  // Debugging
  func dumpData() {
    print("Debug - toggleShowCompleted")
    print("showCompletedItemsFlag=\(showCompletedItemsFlag)")
    print("toDoItems:")
    for todoItem in toDoItems {
      print(" itemText=\(todoItem.itemText), completed=\(todoItem.completed)")
      }
    print("------------------ ")
  }
}

