//
//  ToDoItemTableViewCell.swift
//  SpringToDo
//
//  Created by Steve Greenley on 14/01/16.
//  Copyright © 2016 Swiftly Mobile. All rights reserved.
//

import UIKit

class ToDoItemTableViewCell: UITableViewCell {

  @IBOutlet weak var checkBox: UIButton!
  @IBOutlet weak var itemText: UILabel!
  var delgate: ToDoItemTVCDelegate?
  var rowNumber: Int = 0
  
  @IBAction func toggleCompletion(sender: AnyObject) {
    checkBox.selected = !checkBox.selected
    
    // inform viewcontroller of the change via delegate
    delgate?.setStateOfToDoItemAtRow(rowNumber, completed: checkBox.selected)
  }
  
  var toDoItem: ToDoItem! {
    didSet {
      itemText.text = toDoItem.itemText
      checkBox.selected = toDoItem.completed == true // looks redundant but triggers bridge to NSNumber
    }
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
