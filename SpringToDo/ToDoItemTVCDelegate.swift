//
//  ToDoItemTVCDelegate.swift
//  SpringToDo
//
//  Created by Steve Greenley on 15/01/16.
//  Copyright © 2016 Swiftly Mobile. All rights reserved.
//

import Foundation

protocol ToDoItemTVCDelegate {
  func setStateOfToDoItemAtRow(row: Int, completed:Bool)
}