//
//  ToDoItem+CoreDataProperties.swift
//  SpringToDo
//
//  Created by Steve Greenley on 16/01/16.
//  Copyright © 2016 Swiftly Mobile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ToDoItem {

    @NSManaged var itemText: String?
    @NSManaged var completed: NSNumber?
    @NSManaged var creationDate: NSDate?

}
