# README #

This is a simple To Do list app for Spring written in Swift

Author: Steve Greenley, steve.greenley@gmail.com

## Requirements:

"Let’s have them to a todo list app. It should allow you to:
  * Add a new todo list item (text).
  * Mark items as complete or incomplete.
  * Filter items by completion status.
  * Clear completed items."
  
## Approach:

### UI
  * A simple, single-screen app that presents a list of ToDoItems
  * The most recent items will be shown at the top of the list
  * The title bar will show "To Dos" in the default (Todos) mode
  * Each ToDoItem has a line of text and a checkmark
    * tapping the checkmark will change the item to completed
      * the item will disappear from view
  * The screen will contain a '+' button that allows the user to add a new ToDoItem
    * An alert box with a text field will be used to enter the item text
  * The screen will contain a 'Show Completed' button that applies a filter to show only completed items
    * The title will change to 'Completed Items' while in this (Completed) mode
    * A 'Clear' button will be revealed in this mode that clears all completed items
      * After clearing items, the screen will revert to ToDos mode
    * The "Show Completed" button will change to show "Hide Completed"
      * tapping it will revert to Todos mode
    * tapping the checkmark on a completed ToDoItem will mark it as incomplete
      * the item will disappear from view
      
### Data
  * The list of TodoItems will be stored locally using CoreData
  
### Intentional ommissions
  * Unit tests as this is inferred to be a speed test
  
### Nice to haves:
  * Splash screen - DONE
  * Icon - DONE
  * Video demo - DONE


## General notes
  * Initial estimate 1-2 days
  * Using a navigation controller to make it simpler to extend to multiple screens if necessary
    * this also gives us a titlebar which is convenient given the UI approach above (changes the titlebar text based on filter mode)
  * We'll use a separate nib item for the table cells
  * The tableview will use the main view controller as its datasource and delegate (responder for clicks, etc)
  * Came across a tricky issue with the table showing with a big gap at the top. It turned out that a storyboard setting had been set somehow: "Storyboard.storyboard > ViewController > Attributes inspector > Adjust scroll view insets". Unchecking it fixed the problem.

# Product Backlog - All Done:
  * Implement UI as a storyboard - DONE
	* Create simple (non-coredata) datamodel - DONE
		* Class ToDoItem { boolean: completed, String: itemText } - DONE
	* implement tableview datasource delegate methods - DONE
	* implement add item method - present alertbox - DONE
	* Implment custom tablecell - DONE
	* add call to loaddata to viewdidload - DONE
	* rename viewcontroller to be app specific - DONE
  * make rows non-selectable, only the completion button needs to respond to taps - DONE  
  * Implement action for checkbox button - DONE
	* link checkbox button to IBAction in the cell class - DONE
	* the cell will need to know its row number - use a property in the cell class to store the row number - DONE
	* the cell class will need a callback via a delegate pointer to the viewcontroller - DONE
  * implement showcompleted - DONE
    * apply filter to list & redisplay table - DONE
    * enable clear button - DONE
    * change text of showcompleted button to show "Show To Do List" - DONE
    * change title to show 'completed' - DONE
  * implement clear completed button - DONE
  * Splash screen - DONE
  * Use creation date for sorting on-screen. DONE
  * Core Data persistence - DONE
    * Add Core Data entity - DONE
      * Include creation date
    * generate managed object subclass to replace ToDoItem - DONE
    * Implement loadToDoList() - DONE
      * use fetch object with sort on creation date
    * Implement new object creation - DONE
      * use common Core Data save method
    * Implement item update - DONE
      * on state change to/from completed
      * use common save method
    * Implement completed items deletion - DONE
      * mark managed object for deletion
      * remove item from array
      * use common save method
      * reload data
  * Fix: It is possible to add an item when showing the list of completed items - DONE
    * disable the add button when changing to show completed state
  * Cosmetic improvements
    * colors - use set from Spring Logo and web site - DONE
    * set font auto-resizing for to do items - DONE
    * add icon set for all iOS devices - DONE
    * changed bundle name to match icon "Spring2Do" - DONE
  * Fix: annoying log message about breaking constraints during state change animation - DONE

